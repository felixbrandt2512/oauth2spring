# Vorraussetzungen

- Jeder Dienst über HTTPS abgesichert! OAuth2.0 Bedingung!
- Spring Security

# Implementierung eines Filters zur Zugriffsprüfung eines Nutzers mittels JWT / OAuth2.0

Als Prämisse solle gelten, dass die Services von Personen eingeschränkter Zugriffsberechtigung nicht genutzt werden dürfen.



Für die Umsetzung unter AWS schlage ich folgendes Konzept vor.

![img](https://gitlab.com/felixbrandt2512/oauth2spring/raw/master/bild.PNG)

SPA - Single Page Application

Linke Seite:

Hierbei sind die REST-Dienste direkt von der "Außenwelt" ansprechbar und werden durch einen Tokenvalidierungsprozess geschützt.
D.h. lediglich Nutzer die nach einem erfolgreichen Authentifizierungsprozess ein gütliges Access Token erhielten, können die Daten der Endpunkte abfragen.
Die Verantwortung des Authoriserungsvorganges würde ich dabei auf AWS Cognito weitergeben und keinen eigenen Authorisierungsserver aufbauen.

Rechtes Bild:

Nach weiterer Überlegung, wäre darüber Nachzudenken, ob die Sicherheit weiter erhöht werden könne durch eine Fassaden Struktur bzw. einer Einrichtung eines Proxys der als mittler zu den Apis agieren kann.
Dieser würde nur nach erfolgreicher Validierung die Requests weiterleiten und somit die eigentlichen Endpunkte für die Nutzer verbergen. Diese könnten innerhalb der AWS VPC laufen und wären somit vor direkten Requests von außen geschützt. 

# Rollenkonzept

In diesem Repo befindet sich eine Referenz Implementierung, die mittels einer Filterchain und einer Tokenvalidierung die Nutzungsberechtigung prüft.
Darüber hinaus werden die Nutzerrollen geprüft die unter AWS Cognito eingerichtet werden können.

Somit könnten parametrisierte Abfragen an Datenquellen abgeschickt werden



    @PreAuthorize("hasRole('ROLE_ADMIN')")
	List<Kuchen> getKuchenRezepte();
	
	@PreAuthorize("hasRole('ROLE_HTW')")
	List<Kuchen> getKuchenRezepte2();
	


# Vorteile



    Authorisierung und Validierung von Zugriffsrechten erfolgt von bereits existierenden IdP(identity providern)
    Beispiele(Google,AWS Congnito, Github, ...)
    Zustandslose(state less) Architektur von Backend-Diensten werden möglich.
    -> Session mgt entfiele
    sollte ein eigener Authorisierungsserver aufgesetzt werden müssen, gibt es kostenfreie Tools.(Keycloak)
    Nutzerrollen
    state of the art
    Unterstützung von OpenID zur Nutzeridentifizierung möglich.

# Noch zu Überprüfen

vereinfacht Spring Security 5 integration den Konfigurationsaufwand?

# Usage

git clone https://gitlab.com/felixbrandt2512/oauth2spring.git

Spring boot Application starten.

### POSTMAN Access token abfragen

GET Request anlegen und unter Authorization OAuth2.0 Accesstoken erfragen 

Grant Type: implicit  
Callback URL: http://localhost:8080/home  
Auth URL: https://ospusers.auth.us-east-1.amazoncognito.com/login  
Access Token URL: https://ospusers.auth.us-east-1.amazoncognito.com/oauth2/token    
ID und Secret eintragen  

### POSTMAN Aufruf des Rezept-Endpunktes  

http://localhost:8080/rezepte  
Authorization: Bearer <<ACCESS_TOKEN>>  


# Kostenloses Kontingent für AWS-Cognito

MAU - monthly active user  
"Benutzer gelten als MAUs, wenn innerhalb des jeweiligen Kalendermonats eine Identifizierung der Nutzer erfolgt, etwa aufgrund einer Registrierung, Anmeldung, Tokenaktualisierung oder Passwortänderung."
(https://aws.amazon.com/de/cognito/pricing/, 2018)


Cognito Your User Pool 50 000 MAUs KOSTENLOS  
SAML basierte IdP      50 MAUs


