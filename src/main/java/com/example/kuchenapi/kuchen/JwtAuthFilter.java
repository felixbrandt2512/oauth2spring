package com.example.kuchenapi.kuchen;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.RemoteJWKSet;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;


@ConfigurationProperties("cognito")
public class JwtAuthFilter extends OncePerRequestFilter {

	
	  private static final String AUTH_HEADER_STRING = "Authorization";
	  private static final String AUTH_BEARER_STRING = "Bearer";
	
	  private String ISSUER = "https://cognito-idp.us-east-1.amazonaws.com/us-east-1_iGZBzr68M";
	  private String KEY_STORE_PATH = "/.well-known/jwks.json";
	
	RemoteJWKSet remoteJWKSet;

	public JwtAuthFilter() throws MalformedURLException {
		  URL JWKUrl = new URL(ISSUER + KEY_STORE_PATH);
		  this.remoteJWKSet = new RemoteJWKSet(JWKUrl);
	}
	
	
	
	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws ServletException, IOException {
		
		String header = req.getHeader(AUTH_HEADER_STRING).replace(AUTH_BEARER_STRING,"");

        logger.info(header);

        try {
            JWT jwt = JWTParser.parse(header);

            String iss = jwt.getJWTClaimsSet().getIssuer();
            logger.info(iss);

            // check if issuer is our user pool
            if (ISSUER.equals(jwt.getJWTClaimsSet().getIssuer())) {

            	 File dir = new File("./src/main/resources/aws-token.json");			 
				 
				 JWKSet source = JWKSet.load(dir);
				 ImmutableJWKSet set = new ImmutableJWKSet(source);
				
				 JWSKeySelector keySelector = new JWSVerificationKeySelector(JWSAlgorithm.RS256, set);
				 ConfigurableJWTProcessor jwtProcessor = new DefaultJWTProcessor();				
				 jwtProcessor.setJWSKeySelector(keySelector);

                // check token
                JWTClaimsSet claimsSet = jwtProcessor.process(jwt, null);

                // process roles (gropus in cognito)
                List<String> groups = (List<String>) claimsSet.getClaim("cognito:groups");

                List<GrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new SimpleGrantedAuthority("student"));

                groups.forEach(s -> {
                	authorities.add(new SimpleGrantedAuthority(s));/*
                    logger.info(s);
                    switch (s) {
                        case "ROLE_ADMIN": {
                            authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                        } break;
                        case "HTW_KUECHE": {
                            authorities.add(new SimpleGrantedAuthority("HTW_KUECHE"));
                        } break;
                        case "ROLE_HTW": {
                            authorities.add(new SimpleGrantedAuthority("ROLE_HTW"));
                        }
                        
                    }
                    */
                });

                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                        claimsSet,
                        null,
                        authorities
                );

                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }

        } catch (JOSEException e) {
            e.printStackTrace();
        } catch (BadJOSEException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            // in case that header is null
            e.printStackTrace();
        } catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        chain.doFilter(req, res);

	}

}
