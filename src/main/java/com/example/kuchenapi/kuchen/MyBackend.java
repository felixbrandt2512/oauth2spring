package com.example.kuchenapi.kuchen;

import java.security.Principal;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

public interface MyBackend {	
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	List<Kuchen> getKuchenRezepte();
	
	@PreAuthorize("hasRole('ROLE_HTW')")
	List<Kuchen> getKuchenByCompany(String firmenName);
	
	
	
}
