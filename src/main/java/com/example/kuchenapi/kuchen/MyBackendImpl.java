package com.example.kuchenapi.kuchen;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class MyBackendImpl implements MyBackend {
	
	@Autowired
	Kuchenrepo repo;
	
	@Override
	public List<Kuchen> getKuchenRezepte() {
		return repo.findAll();
	}

	@Override
	public List<Kuchen> getKuchenByCompany(String firmenName) {	
		System.out.println("Name: "+SecurityContextHolder.getContext().getAuthentication().getName());
		return repo.findByFirma(firmenName);
	}
	
}
