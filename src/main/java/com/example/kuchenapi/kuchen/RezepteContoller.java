package com.example.kuchenapi.kuchen;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nimbusds.jwt.JWTClaimsSet;

import net.minidev.json.JSONArray;

@RestController
public class RezepteContoller {
	
	@Autowired
	MyBackend repo;

	@RequestMapping("/rezepte")
	public List<Kuchen> getHTWKuchenRezepte(Authentication princ){
		JWTClaimsSet claims = (JWTClaimsSet) princ.getPrincipal();
		System.out.println("Claims: "+claims.getClaim("cognito:groups"));
		return repo.getKuchenRezepte();
	}
	
	@RequestMapping("/rezeptehtw")
	public List<Kuchen> getHTWKuchenRezepte2(Authentication princ){
		JWTClaimsSet claims = (JWTClaimsSet) princ.getPrincipal();
		System.out.println("Claims: "+claims.getClaim("cognito:groups"));
		return repo.getKuchenByCompany("bla");
	}
	
	
	
	@RequestMapping(value = "/companies/{CompanyName}/rezepte",  method = RequestMethod.GET)
	public List<Kuchen> getRezeptebyCompany(Authentication princ,@PathVariable(value  = "CompanyName")  String firmenname){	
		List<Kuchen> kuchen = new ArrayList();
		JWTClaimsSet claims = (JWTClaimsSet) princ.getPrincipal();
		System.out.println("Claims: "+claims.getClaim("cognito:groups"));
		JSONArray set = (JSONArray) claims.getClaim("cognito:groups");
		
		if(set.contains(firmenname)) {
			kuchen = repo.getKuchenByCompany(firmenname);
		}				
		return kuchen;
		
	}
	
	
}
