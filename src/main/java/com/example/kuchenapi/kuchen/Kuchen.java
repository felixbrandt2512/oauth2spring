package com.example.kuchenapi.kuchen;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Kuchen {
	
	@Id
    private Long id;
	private String Name;
	private String firma;
	
	public Kuchen() {
		
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}
	
	
	public Kuchen(String name) {
		this.setName(name);
	}
}
