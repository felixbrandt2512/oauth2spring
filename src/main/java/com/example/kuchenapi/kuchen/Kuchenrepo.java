package com.example.kuchenapi.kuchen;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface Kuchenrepo extends JpaRepository<Kuchen,Long>{
  List<Kuchen> findByFirma(String firmenname);
}
